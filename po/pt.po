# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Nick Schermer <nick@xfce.org>, 2019
# Nuno Miguel <nunomgue@gmail.com>, 2020
# José Vieira <jvieira33@sapo.pt>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-02 00:46+0200\n"
"PO-Revision-Date: 2019-01-14 12:47+0000\n"
"Last-Translator: José Vieira <jvieira33@sapo.pt>, 2020\n"
"Language-Team: Portuguese (https://www.transifex.com/xfce/teams/16840/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/calculator.c:156
#, c-format
msgid "Calculator error: %s"
msgstr "Erro da calculadora: %s"

#: ../panel-plugin/calculator.c:233
msgid " Calc:"
msgstr "Calcular:"

#: ../panel-plugin/calculator.c:417
msgid "Calculator Plugin"
msgstr "Plugin calculadora"

#. add the "Close" button
#: ../panel-plugin/calculator.c:420 ../panel-plugin/calculator.c:497
msgid "_Close"
msgstr "_Fechar"

#. Appearance
#: ../panel-plugin/calculator.c:439
msgid "Appearance"
msgstr "Aparência"

#: ../panel-plugin/calculator.c:449
msgid "Width (in chars):"
msgstr "Largura (em caracteres):"

#. History
#: ../panel-plugin/calculator.c:462
msgid "History"
msgstr "Histórico"

#: ../panel-plugin/calculator.c:472
msgid "Size:"
msgstr "Tamanho:"

#. Behavior
#: ../panel-plugin/calculator.c:483
msgid "Behavior"
msgstr "Comportamento"

#: ../panel-plugin/calculator.c:489
msgid "Do not move cursor after calculation"
msgstr "Após o cálculo, não mover o cursor"

#: ../panel-plugin/calculator.c:523
msgid "Calculator for Xfce panel"
msgstr "Calculadora para o painel Xfce"

#: ../panel-plugin/calculator.c:525
msgid "Copyright (c) 2003-2019\n"
msgstr "Copyright (c) 2003-2019\n"

#: ../panel-plugin/calculator.c:575
msgid "Trigonometrics use degrees"
msgstr "Usar graus trignométricos"

#: ../panel-plugin/calculator.c:578
msgid "Trigonometrics use radians"
msgstr "Usar radianos trignométricos"

#. Add checkbox to enable hexadecimal output
#: ../panel-plugin/calculator.c:598
msgid "Hexadecimal output"
msgstr "Saída hexadecimal"

#: ../panel-plugin/calculator.desktop.in.h:1
msgid "Calculator"
msgstr "Calculadora"

#: ../panel-plugin/calculator.desktop.in.h:2
msgid "Calculator plugin for the Xfce panel"
msgstr "Plugin calculadora para o painel Xfce"
