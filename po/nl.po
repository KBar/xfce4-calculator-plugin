# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Nick Schermer <nick@xfce.org>, 2019
# Pjotr <pjotrvertaalt@gmail.com>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-02 00:46+0200\n"
"PO-Revision-Date: 2019-01-14 12:47+0000\n"
"Last-Translator: Pjotr <pjotrvertaalt@gmail.com>, 2020\n"
"Language-Team: Dutch (https://www.transifex.com/xfce/teams/16840/nl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/calculator.c:156
#, c-format
msgid "Calculator error: %s"
msgstr "Fout van rekenmachine: %s"

#: ../panel-plugin/calculator.c:233
msgid " Calc:"
msgstr "Reken:"

#: ../panel-plugin/calculator.c:417
msgid "Calculator Plugin"
msgstr "Invoegsel voor rekenmachine"

#. add the "Close" button
#: ../panel-plugin/calculator.c:420 ../panel-plugin/calculator.c:497
msgid "_Close"
msgstr "S_luiten"

#. Appearance
#: ../panel-plugin/calculator.c:439
msgid "Appearance"
msgstr "Uiterlijk"

#: ../panel-plugin/calculator.c:449
msgid "Width (in chars):"
msgstr "Breedte (in tekens):"

#. History
#: ../panel-plugin/calculator.c:462
msgid "History"
msgstr "Geschiedenis"

#: ../panel-plugin/calculator.c:472
msgid "Size:"
msgstr "Grootte:"

#. Behavior
#: ../panel-plugin/calculator.c:483
msgid "Behavior"
msgstr "Gedrag"

#: ../panel-plugin/calculator.c:489
msgid "Do not move cursor after calculation"
msgstr "Verplaats de aanwijzer niet na de berekening"

#: ../panel-plugin/calculator.c:523
msgid "Calculator for Xfce panel"
msgstr "Rekenmachine voor Xfce-werkbalk"

#: ../panel-plugin/calculator.c:525
msgid "Copyright (c) 2003-2019\n"
msgstr "Auteursrecht (c) 2003-2019\n"

#: ../panel-plugin/calculator.c:575
msgid "Trigonometrics use degrees"
msgstr "Trigonometrie gebruikt graden"

#: ../panel-plugin/calculator.c:578
msgid "Trigonometrics use radians"
msgstr "Trigonometrie gebruikt radialen"

#. Add checkbox to enable hexadecimal output
#: ../panel-plugin/calculator.c:598
msgid "Hexadecimal output"
msgstr "Hexadecimale uitvoer"

#: ../panel-plugin/calculator.desktop.in.h:1
msgid "Calculator"
msgstr "Rekenmachine"

#: ../panel-plugin/calculator.desktop.in.h:2
msgid "Calculator plugin for the Xfce panel"
msgstr "Rekenmachine-invoegsel voor de Xfce-werkbalk"
